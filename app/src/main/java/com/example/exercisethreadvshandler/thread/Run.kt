package com.example.exercisethreadvshandler.thread

import android.util.Log

class Run(private var id: Int) : Runnable {
    override fun run() {
        Log.i("TAG","Thread $id đang thực thi")
    }
}
