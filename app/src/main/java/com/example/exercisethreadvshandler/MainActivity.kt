package com.example.exercisethreadvshandler

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.exercisethreadvshandler.thread.Run
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity(), View.OnClickListener {
    private var mIsCounting = true
    lateinit var mHandler: Handler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        listenerHandler()
    }

    override fun onResume() {
        super.onResume()
        button_count.setOnClickListener(this)
        buttonStartThreadPool.setOnClickListener(this)
    }

    private fun listenerHandler() {
        mHandler = object : Handler(Looper.getMainLooper()) {
            override fun handleMessage(msg: Message) {
                when (msg.what) {
                    MGS_UPDATE_NUMBER -> {
                        mIsCounting = true
                        text_number.setText(msg.arg1.toString())
                    }
                    MGS_UPDATE_NUMBER_DONE -> {
                        text_number.setText("Done!")
                        mIsCounting = false
                    }
                    else -> {
                    }
                }
            }
        }
    }

    private fun countNumbers() {
        Thread {
            for (i in 0..10) {
                val message = Message()
                message.what = MGS_UPDATE_NUMBER
                message.arg1 = i
                mHandler.sendMessage(message)
                try {
                    Thread.sleep(1000)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
            }
            mHandler.sendEmptyMessage(MGS_UPDATE_NUMBER_DONE)
            mHandler.post(object : Runnable {
                /**
                 * When an object implementing interface `Runnable` is used
                 * to create a thread, starting the thread causes the object's
                 * `run` method to be called in that separately executing
                 * thread.
                 *
                 *
                 * The general contract of the method `run` is that it may
                 * take any action whatsoever.
                 *
                 * @see java.lang.Thread.run
                 */
                override fun run() {
                    TODO("Not yet implemented")
                }

            })
        }.start()
    }

    companion object {
        const val MGS_UPDATE_NUMBER = 100
        const val MGS_UPDATE_NUMBER_DONE = 101
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.button_count -> {
                Log.i("TAG", "onClick")
                if (mIsCounting) {
                    countNumbers()
                }
            }
            R.id.buttonStartThreadPool -> {
                // Cách làm thủ công
//                val corePoolSize = 5
//                val maximumPoolSize = 10
//                val keepAliveTime = 500L
//                val unit = TimeUnit.SECONDS
//                val workQuere = ArrayBlockingQueue<Runnable>(100)
//                val handler = ThreadPoolExecutor.CallerRunsPolicy()
//                val threadPoolExcutor = ThreadPoolExecutor(
//                    corePoolSize,
//                    maximumPoolSize,
//                    keepAliveTime,
//                    unit,
//                    workQuere,
//                    handler
//                )
//                for (i in 0..9) {
//                    threadPoolExcutor.execute(Run(i))
//                }
                //Cách sử dụng Executor Service
                val pool = Executors.newFixedThreadPool(5)
                //Single Thread Excutor.
                //Cached Thread Pool.
                //Fixed Thread Pool.
                //Scheduled Thread Pool.
                //Single Thread Scheduled Pool.
                for (i in 0..9) {
                    pool.submit(Run(i))
                    //execute.
                    //submit(runnable)
                    //submit(callable).
                    //invoke any
                }
                pool.shutdown()
                pool.awaitTermination(1, TimeUnit.DAYS)
                //shutdown.
                //shutdownNow.
                //awaitTermination.
            }
        }
    }
}
